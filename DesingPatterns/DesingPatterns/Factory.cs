﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;
using DesignPatterns.Creational.Trabalho1.FactoryMethod;
using DesignPatterns.Creational.Trabalho1.Builder;

namespace DesignPatterns.Creational.Trabalho1
{
    public sealed class Factory
    {
        private Factory() { }

        private static Factory instance = null;

        private static Computer allInOne;
        private static Computer notebookFactory;

        public string Name;

        public static Factory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Factory();
                    allInOne = new AllInOneFactory().GetComputer();
                    notebookFactory = new NotebookFactory().GetComputer();
                }
                return instance;
            }
        }

        public Computer GetAllInOne { get => allInOne; }

        public Computer GetNotebook { get => notebookFactory; }
    }

}
