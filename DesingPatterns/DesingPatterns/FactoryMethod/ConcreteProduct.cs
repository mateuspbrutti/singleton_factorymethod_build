﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Creational.Trabalho1.Builder;

namespace DesignPatterns.Creational.Trabalho1.FactoryMethod
{
    public class ConcreteProduct : Product
    {
        public override Computer computer { get; protected set; }

        public ConcreteProduct(Computer comp)
        {
            computer = comp;
        }
    }

    public class AllInOne : Product
    {
        public override Computer computer { get; protected set; }

        public AllInOne(Computer comp)
        {
            computer = comp;
        }       
    }

}
