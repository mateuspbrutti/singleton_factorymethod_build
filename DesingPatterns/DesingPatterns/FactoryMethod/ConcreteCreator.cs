﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;
using DesignPatterns.Creational.Trabalho1.Builder;

namespace DesignPatterns.Creational.Trabalho1.FactoryMethod
{
    public class AllInOneFactory : Creator
    {
        private Computer computer = new Computer();
        
        public AllInOneFactory()
        {
            CPU cpu = new CPU();
            cpu.Brancd = BrandName.Intel;
            cpu.Clock = 3200;

            Monitor monitor = new Monitor();
            monitor.Inches = 23;
            monitor.WideScreen = true;

            computer.Model = "AllInOne";
            computer.Ram = RamType.GB8;
            computer.Cpu = cpu;
            computer.Monitor = monitor;
        }
        
        public override Computer GetComputer() { return computer; }
    }

    public class NotebookFactory : Creator
    {
        private Computer computer = new Computer();

        public NotebookFactory()
        {
            CPU cpu = new CPU();
            cpu.Brancd = BrandName.AMD;
            cpu.Clock = 3900;

            Monitor monitor = new Monitor();
            monitor.Inches = 15.6f;
            monitor.WideScreen = true;

            computer.Model = "NoteBook";
            computer.Ram = RamType.GB16;
            computer.Cpu = cpu;
            computer.Monitor = monitor;
        }

        public override Computer GetComputer() { return computer; }
    }

}

