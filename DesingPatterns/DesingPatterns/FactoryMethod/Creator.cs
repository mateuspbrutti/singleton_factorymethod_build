﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;
using DesignPatterns.Creational.Trabalho1.Builder;

namespace DesignPatterns.Creational.Trabalho1.FactoryMethod
{
    public abstract class Creator
    {
        public abstract Computer GetComputer();
    }
}
