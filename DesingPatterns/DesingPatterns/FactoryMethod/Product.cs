﻿using DesignPatterns.Creational.Trabalho1.Builder;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1.FactoryMethod
{
    public abstract class Product
    {
        public abstract Computer computer { get; protected set; }

        public Product()
        {
            computer = new ConcreteBuilder().GetComputer();
        }

    }

}
