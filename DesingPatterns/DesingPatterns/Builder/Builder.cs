﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1.Builder
{
    public interface IComputerBuilder
    {
        void SetModel(string model);
        void SetRam(RamType ramType);
        void SetCPU(CPU cpu);
        void SetMonitor(Monitor monitor);

        Computer GetComputer();
    }
}
