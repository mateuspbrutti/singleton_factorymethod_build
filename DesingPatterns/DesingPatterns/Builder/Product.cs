﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1.Builder
{
    public enum RamType
    {
        [StringValue("2GB")]
        GB2,
        [StringValue("4GB")]
        GB4,
        [StringValue("8GB")]
        GB8,
        [StringValue("16GB")]
        GB16
    }
    
    public class Computer
    {
        public string Model { get; set; }

        public RamType Ram { get; set; }

        public CPU Cpu { get; set; }

        public Monitor Monitor { get; set; }

        public Computer()
        {
            Cpu = new CPU();
            Monitor = new Monitor();
        }

    }
    
}
