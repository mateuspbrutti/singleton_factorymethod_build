﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System;

namespace DesignPatterns.Creational.Trabalho1.Builder
{
    public class Monitor
    {
        public float Inches { get; set; }
        public bool WideScreen { get; set; }
    }
}
