﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1.Builder
{
    public enum BrandName
    {
        Intel, AMD
    }

    public class CPU
    {
        public BrandName Brancd { get; set; }
        public int Clock { get; set; }
    }
}
