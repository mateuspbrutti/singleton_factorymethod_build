﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1.Builder
{
    public class ConcreteBuilder : IComputerBuilder
    {
        Computer objComputer = new Computer();
        
        public Computer GetComputer()
        {
           return objComputer;
        }

        public void SetModel(string model)
        {
            objComputer.Model = model;
        }

        public void SetCPU(CPU cpu)
        {
            objComputer.Cpu = cpu;
        }
        
        public void SetMonitor(Monitor monitor)
        {
            objComputer.Monitor = monitor;
        }

        public void SetRam(RamType ramType)
        {
            objComputer.Ram = ramType;
        }
    }
}
