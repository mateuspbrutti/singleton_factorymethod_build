﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1.Builder
{
    public class ComputerCreator
    {
        private readonly IComputerBuilder objBuilder;

        public ComputerCreator(IComputerBuilder builder)
        {
            objBuilder = builder;
        }

        public void CreateComputer(string model, RamType ramType, CPU cpu, Monitor monitor)
        {
            objBuilder.SetModel(model);
            objBuilder.SetRam(ramType);
            objBuilder.SetCPU(cpu);
            objBuilder.SetMonitor(monitor);
        }

        public Computer GetVehicle()
        {
            return objBuilder.GetComputer();
        }
    }

}
