﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesignPatterns.Creational.Trabalho1
{
    public class StringValueAttribute : Attribute
    {
        public string stringValue { get; protected set; }
           
        public StringValueAttribute(string value)
        {
            stringValue = value;
        }
    }
}
