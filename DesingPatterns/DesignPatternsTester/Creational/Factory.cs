﻿using DesignPatterns.Creational.Trabalho1.FactoryMethod;
using DesignPatterns.Creational.Trabalho1.Builder;
using DesignPatterns.Creational.Trabalho1;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesingPatternsTeste.Creational
{
    class ComputerFactory
    {
        public void Run()
        {
            Factory fac = Factory.Instance;

            Console.WriteLine("\nComputer One\n");           
            fac.Name = "DELL";

            Computer alIn = fac.GetAllInOne;

            Console.WriteLine(string.Format("\t {0} \nMODEL: {1}\nRAM: {2}\nCPU:\n    BRENCD: {3}\n    CLOCK: {4}MHz\nMONITOR:\n    INCHES: {5}\n    WIDESCREEN: {6}",
                fac.Name, alIn.Model, alIn.Ram.ToString(), alIn.Cpu.Brancd.ToString(), alIn.Cpu.Clock, alIn.Monitor.Inches, alIn.Monitor.WideScreen)
                );


            Console.WriteLine("\n\nComputer Two\n");
            fac.Name = "ACER";

            Computer note = fac.GetNotebook;            

            Console.WriteLine(string.Format("\t {0} \nMODEL: {1}\nRAM: {2}\nCPU:\n    BRENCD: {3}\n    CLOCK: {4}MHz\nMONITOR:\n    INCHES: {5}\n    WIDESCREEN: {6}",
                fac.Name, note.Model, note.Ram.ToString(), note.Cpu.Brancd.ToString(), note.Cpu.Clock, note.Monitor.Inches, note.Monitor.WideScreen)
                );

        }

    }

}
