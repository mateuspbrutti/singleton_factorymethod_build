﻿using DesingPatternsTeste.Creational;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;

namespace DesingPatternsTeste
{
    class Program
    {
        static void Main(string[] args)
        {
            CreationalFactoryComputer();
            Console.ReadKey();
        }


        private static void CreationalFactoryComputer()
        {
            ComputerFactory cf = new ComputerFactory();
            cf.Run();
        }

    }
}
